﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Practice_2_3 : MonoBehaviour
{
    private int EntityHP = 500;
    private float EntityMana = 100f;
    private string EntityName = "Doragon";
    private bool IsEntityAlive = true;

    public void Start()
    {
        GetLove();
        GetAge();
        CurrentInfo();

        int DoragonHealth = GetHealth();
        bool DoragonStatus = GetStatus();
    }

    public void GetLove()
    {
        return;
    }

    public void GetAge(float CurrentAge = 150f)
    {
        return;
    }

    public void CurrentInfo(string CurrentName = "Kobayashi", bool IsEntityAFreeLoader = true, int CurrentMoney = 0)
    {
        return;
    }

    public void EntityInfo(int CurrentHP, float CurrentMana, string CurrentName)
    {
        CurrentHP = EntityHP;
        CurrentMana = EntityMana;
        CurrentName = EntityName;
        return;
    }

    public int GetHealth()
    {
        return EntityHP;
    }

    public bool GetStatus()
    {
        return IsEntityAlive;
    }
}