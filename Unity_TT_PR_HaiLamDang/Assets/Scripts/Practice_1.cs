﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Practice_1 : MonoBehaviour
{
    public class Entity
    {

    }

    public class Wolf
    {

    }

    public class Chicken
    {

    }

    public class Cat
    {

    }

    public class Dog
    {

    }

    public class Rat
    {

    }

    //second excercise 
    public class Enemy
    {

    }

    public class EnemySoilder : Enemy
    {
        public int HitPoint = 100;
        public int ManaPoint = 50;

        public float Gold = 10.5f;
        public float Age = 25.6f;

        public string Name = "John";
        public string PetType = "Cat";
        public string Job = "Soilder";

        public bool IsEnemyAlive = true;
        public bool IsEnemyEvil = true;
        public bool IsEnemySingle = false;
        public bool CanEnemyUseMagic = false;
    }

    public class EnemyGeneral : Enemy
    {
        public int HitPoint = 150;
        public int ManaPoint = 100;

        public float Gold = 30.5f;
        public float Age = 40.1f;

        public string Name = "Bob";
        public string PetType = "Wolf";
        public string Job = "High General";

        public bool IsEnemyAlive = true;
        public bool IsEnemyEvil = true;
        public bool IsEnemySingle = true;
        public bool CanEnemyUseMagic = true;
    }

    public class EnemyMage : Enemy
    {
        public int HitPoint = 100;
        public int ManaPoint = 200;

        public float Gold = 100.8f;
        public float Age = 60.7f;

        public string Name = "Arron";
        public string PetType = "Rat";
        public string Job = "Royal Mage";

        public bool IsEnemyAlive = false;
        public bool IsEnemyEvil = true;
        public bool IsEnemySingle = true;
        public bool CanEnemyUseMagic = true;
    }

    public class EnemyKing : Enemy
    {
        public int HitPoint = 300;
        public int ManaPoint = 50;

        public float Gold = 1000f;
        public float Age = 40.1f;

        public string Name = "Rex The Great";
        public string PetType = "Chicken";
        public string Job = "His Royal HIghness";

        public bool IsEnemyAlive = true;
        public bool IsEnemyEvil = true;
        public bool IsEnemySingle = false;
        public bool CanEnemyUseMagic = true;
    }

    public class EnemyDragon : Enemy
    {
        public int HitPoint = 1000;
        public int ManaPoint = 500;

        public float Gold = 500.5f;
        public float Age = 2000.9f;

        public string Name = "Scrubby";
        public string PetType = "Human";
        public string Job = "Giant Lizard hoarding Gold";

        public bool IsEnemyAlive = true;
        public bool IsEnemyEvil = true;
        public bool IsEnemySingle = true;
        public bool CanEnemyUseMagic = true;
    }
}
