﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Practice_3 : MonoBehaviour
{
    private int EntityHP = 100; //Health
    private int EntityMana = 100; //Mana
    private float EntityAge = 25f; //Age
    private bool IsEntityAlive = true; //Alive status? Yes
    private string EntityName = "Isana"; //Name
    public int GetHealth
    {
        get { return EntityHP; }
        set { EntityHP = value; }
    }

    public int GetMana
    {
        get { return EntityMana; }
        set { EntityMana = value; }
    }

    public float GetAge
    {
        get { return EntityAge; }
        set { EntityAge = value; }
    }

    public bool GetEntityAliveStatus
    {
        get { return IsEntityAlive; }
        set { IsEntityAlive = value; }
    }

    public string GetName
    {
        get { return EntityName; }
        set { EntityName = value; }
    }

    public void InitRank() //show Entity's Rank
    {
        return;
    }

    public void InitHealth(int SampleHealth)
    {
        return;
    }
}

public class SecondEntity
{
    private int SecondEntityHP = 200;
    private int SecondEntityMana = 200;

    public int GetHP
    {
        get { return SecondEntityHP; }
    }

    public int GetMana
    {
        get { return SecondEntityMana; }
    }

    private bool IsSecondEntityAlive = true;
    private string SecondEntityName = "Timer20";

    public bool SetSecondAliveStatus
    {
        set { IsSecondEntityAlive = value; }
    }

    public string GetName
    {
        set { SecondEntityName = value; }
    }
}

public class ThirdEntity : Practice_3
{
    public Practice_3 HandCream;

    public void Start()
    {
        int health;
        int mana;
        float age;
        bool alive;
        string name;

        health = HandCream.GetHealth;
        mana = HandCream.GetMana;
        age = HandCream.GetAge;
        alive = HandCream.GetEntityAliveStatus;
        name = HandCream.GetName;

        print();
    }

    public void print()
    {
        Debug.Log("Show HP:" + GetHealth);
        Debug.Log("Show Mana:" + GetMana);
        Debug.Log("Show Age:" + GetAge);
        Debug.Log("Show Alive Status:" + GetEntityAliveStatus);
        Debug.Log("Show Name:" + GetName);
    }
}