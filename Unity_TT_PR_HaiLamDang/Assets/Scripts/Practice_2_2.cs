﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Practice_2_2 : MonoBehaviour
{
    public int EntityHP = 100;
    public int EntityMana = 100;
    public float EntityAge = 27.8f;
    public string EntityName = "Alexander";
    public bool IsEntityAlive = true;

    public void Start()
    {
        PrintHP();
        PrintMana();
        PrintAge();
        PrintName();
        PrintAliveStatus();
    }

    public void PrintHP()
    {
        Debug.Log("HP" + EntityHP);
    }

    public void PrintMana()
    {
        Debug.Log("Mana" + EntityMana);
    }

    public void PrintAge()
    {
        Debug.Log("Age" + EntityAge);
    }

    public void PrintName()
    {
        Debug.Log("Name" + EntityName);
    }

    public void PrintAliveStatus()
    {
        Debug.Log("Alive Status" + IsEntityAlive);
    }

}