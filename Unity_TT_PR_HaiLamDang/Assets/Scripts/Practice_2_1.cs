﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Practice_2_1 : MonoBehaviour
{
    private int EntityMoney = 40;
    private float EntityAge = 15.2f;
    private bool IsEntityAlive = true;
    private string EntityName = "Timothy";
    private string EntityPet = "Dog";

    public int EntityClass = 5;
    public float EntityStocks = 0;
    public bool DoesEntityHaveSchool = true;
    public string EntityNickName = "Timmy";
    public string EntityPetName = "Lucky";

    public void Start()
    {
        Print();
    }

    public void Print()
    {
        Debug.Log("Money:" + EntityMoney);
        Debug.Log("Age:" + EntityAge);
        Debug.Log("Alive status:" + IsEntityAlive);
        Debug.Log("Name:" + EntityName);
        Debug.Log("Pet type:" + EntityPet);

        Debug.Log("Class:" + EntityClass);
        Debug.Log("Stocks:" + EntityStocks);
        Debug.Log("School status:" + DoesEntityHaveSchool);
        Debug.Log("Nickname:" + EntityNickName);
        Debug.Log("Pet name:" + EntityPetName);
    }
} 